package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.logindao;
import model.User;

/**
 * Servlet implementation class UserDetailServlet
 */
@WebServlet("/UserDetailServlet")
public class UserDetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserDetailServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(false);

		if(session != null) {
			User user = (User)session.getAttribute("userInfo");
			if (user == null)
			{
				response.sendRedirect("LoginServlet");
				return ;
			}
			session = request.getSession(true);
			// ユーザ一覧情報を取得
		request.setCharacterEncoding("UTF-8");
		User user1 = (User)session.getAttribute("userInfo");
		session.setAttribute("userInfo", user1);

		// URLからGETパラメータとしてIDを受け取る
		String id = request.getParameter("id");

		// 確認用：idをコンソールに出力
		System.out.println(id);
		// TODO  未実装：idを引数にして、idに紐づくユーザ情報を出力する
		// TODO  未実装：ユーザ情報をリクエストスコープにセットしてjspにフォワード
		if (id.length() == 0 )
		{
			//error
			// 更新がめんjspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/UserUpdate.jsp");
			dispatcher.forward(request, response);
			return;
		}else {
			int dbId = Integer.parseInt(id);
			// DB
			logindao userDao1 = new logindao();
			User user11 = userDao1.findById(dbId);

			// Servletのデータ受け取り
			request.setCharacterEncoding("UTF8");
			request.setAttribute("fromServlet",user11);

			// 更新がめんjspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/UserDetail.jsp");
			dispatcher.forward(request, response);
			return;
		}
		}else {
		response.sendRedirect("LoginServlet");
		}
	}
}

