package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserCreateServlet
 */
@WebServlet("/UserCreateServlet")
public class UserCreateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserCreateServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession(false);

		if (session != null) {
			User user = (User) session.getAttribute("userInfo");
			if (user == null) {
				response.sendRedirect("LoginServlet");
				return;
			}

			session = request.getSession(true);
			// ユーザ一覧情報を取得
			UserDao userDao = new UserDao();
			List<User> userList = userDao.findAll();

			// リクエストスコープにユーザ一覧情報をセット
			request.setAttribute("userList", userList);
			request.setCharacterEncoding("UTF-8");

			User user1 = (User) session.getAttribute("userInfo");
			session.setAttribute("userInfo", user1);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/UserAdd.jsp");
			dispatcher.forward(request, response);
		} else {
			response.sendRedirect("LoginServlet");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		request.setCharacterEncoding("UTF-8");
		User user = (User) session.getAttribute("userInfo");
		session.setAttribute("userInfo", user);

		String loginId = request.getParameter("loginId");
		String name = request.getParameter("userName");
		String birthDate = request.getParameter("birthDate");
		String password1 = request.getParameter("password");
		String password2 = request.getParameter("passwordConf");

		UserDao daoArray = new UserDao();

		List<User> listInfo = daoArray.findAll();
		request.setAttribute("list", listInfo);
		if (!password1.equals(password2) ||
				(name.length() == 0 &&
						birthDate.length() == 0)) {
			// DB
			// Servletのデータ受け取り
			request.setCharacterEncoding("UTF8");
			// ・更新に失敗した場合、入力画面に戻る。タイトルの下に赤色で「入力された内容は正しくありません」という文言を表示する。
			request.setAttribute("errMsg", "入力された内容は正しくありません。");
			// 更新がめんjspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/UserAdd.jsp");
			dispatcher.forward(request, response);
			return;
		}
		UserDao dao = new UserDao();
		try {
			dao.UserCreate(loginId, name, birthDate, password1);
			//・登録に成功した場合、ユーザ一覧画面に遷移する。
			// ユーザ一覧のサーブレットにリダイレクト
			response.sendRedirect("UserListServlet");
		}catch(Exception e) {
			request.setAttribute("errMsg", "更新に失敗しました。");
			// 更新がめんjspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/UserAdd.jsp");
			dispatcher.forward(request, response);
			return;
		}

	}
}