package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import dao.logindao;
import model.User;

/**
 * Servlet implementation class LogoutServlet
 */
@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserUpdateServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO 未実装：ログインセッションがない場合、ログイン画面にリダイレクトさせる
		// セッションにユーザの情報をセット
		HttpSession session = request.getSession(false);

		if (session != null) {
			User user = (User) session.getAttribute("userInfo");
			if (user == null) {
				response.sendRedirect("LoginServlet");
				return;
			}

			session = request.getSession(true);
			// ユーザ一覧情報を取得
			UserDao userDao = new UserDao();
			List<User> userList = userDao.findAll();

			// リクエストスコープにユーザ一覧情報をセット
			request.setAttribute("userList", userList);
			request.setCharacterEncoding("UTF-8");

			User user1 = (User) session.getAttribute("userInfo");
			session.setAttribute("userInfo", user1);

			// リクエストパラメータの入力項目を取得
			String id = request.getParameter("id");
			if (id.length() == 0) {
				//error
				// 更新がめんjspにフォワード
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/UserUpdata.jsp");
				dispatcher.forward(request, response);
				return;
			} else {
				int dbId = Integer.parseInt(id);
				// DB
				logindao userDao1 = new logindao();
				User user11 = userDao1.findById(dbId);
				// Servletのデータ受け取り
				request.setCharacterEncoding("UTF8");
				request.setAttribute("fromServlet", user11);

				// 更新がめんjspにフォワード
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/UserUpdata.jsp");
				dispatcher.forward(request, response);
				return;
			}
		} else {
			response.sendRedirect("LoginServlet");
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		request.setCharacterEncoding("UTF-8");
		User user1 = (User) session.getAttribute("userInfo");
		session.setAttribute("userInfo", user1);

		String id = request.getParameter("id");
		String password1 = request.getParameter("password");
		String password2 = request.getParameter("passwordConf");
		String userName = request.getParameter("userName");
		String birthDate = request.getParameter("birthDate");
		UserDao daoArray = new UserDao();


		List<User> listInfo = daoArray.findAll();
		request.setAttribute("list", listInfo);
		//・以下の場合は、更新失敗としてデータの更新は行わない。
		//　- パスワードとパスワード(確認)の入力内容が異なる場合
		//　- パスワード以外に未入力の項目がある場合。
		// ・パスワードとパスワード(確認)がどちらも空欄の場合は、パスワードは更新せずにパスワード以外の項目を更新する。(未実装)
		if (!password1.equals(password2) ||
				(userName.length() == 0 &&
			    birthDate.length() == 0)) {
			//先に異常表示
			int dbId = Integer.parseInt(id);
			// DB
			logindao userDao = new logindao();
			User user = userDao.findById(dbId);
			// Servletのデータ受け取り
			request.setCharacterEncoding("UTF8");
			request.setAttribute("fromServlet", user);
			// ・更新に失敗した場合、入力画面に戻る。タイトルの下に赤色で「入力された内容は正しくありません」という文言を表示する。
			request.setAttribute("errMsg", "入力された内容は正しくありません。");
			// 更新がめんjspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/UserUpdata.jsp");
			dispatcher.forward(request, response);
			return;
		}
		else {
			//正常処理
			//パスワードの空欄だった場合
			UserDao userDao = new UserDao();
			if( password1.length()==0 && password2.length()==0 &&
				(userName.length() > 0 && birthDate.length() > 0))
			{
				try {
					userDao.UserUpPassNull(id, userName, birthDate);
					//・登録に成功した場合、ユーザ一覧画面に遷移する。
					// ユーザ一覧のサーブレットにリダイレクト
					response.sendRedirect("UserListServlet");
				} catch (Exception e) {
					request.setAttribute("errMsg", "更新処置に失敗しました。");
					// 更新がめんjspにフォワード
					RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/UserUpdata.jsp");
					dispatcher.forward(request, response);
					return;
				}
			}else {
				//パスワードが入っている場合
				try {
					userDao.UserUpdate(id, userName, birthDate, password1);
					//・登録に成功した場合、ユーザ一覧画面に遷移する。
					// ユーザ一覧のサーブレットにリダイレクト
					response.sendRedirect("UserListServlet");
				} catch (Exception e) {
					request.setAttribute("errMsg", "更新処置に失敗しました。");
					// 更新がめんjspにフォワード
					RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/UserUpdata.jsp");
					dispatcher.forward(request, response);
					return;
				}
			}
		}
	}
}
