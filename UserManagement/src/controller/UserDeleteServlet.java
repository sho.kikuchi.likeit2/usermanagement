package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import dao.logindao;
import model.User;

/**
 * Servlet implementation classUserDeleteServlet
 */
@WebServlet("/UserDeleteServlet")
public class UserDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserDeleteServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession(false);
		if(session != null) {
			User user = (User)session.getAttribute("userInfo");
			if (user == null)
			{
				response.sendRedirect("LoginServlet");
				return ;
			}

			session = request.getSession(true);
		request.setCharacterEncoding("UTF-8");

		// リクエストパラメータの入力項目を取得
		String id = request.getParameter("id");

		// DB
		if (id.length() == 0) {
			//error
			// 更新がめんjspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/UserUpdate.jsp");
			dispatcher.forward(request, response);
			return;
		} else {
			int dbId = Integer.parseInt(id);
			// DB
			logindao userDao1 = new logindao();
			User user1 = userDao1.findById(dbId);

			// Servletのデータ受け取り
			request.setAttribute("fromServlet",user1);

			// デリートがめんjspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/UserDelete.jsp");
			dispatcher.forward(request, response);
			return;
		}
		}else {
		response.sendRedirect("LoginServlet");
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		request.setCharacterEncoding("UTF-8");

		String id = request.getParameter("id");

		if (id.length() > 0 )
		{
			try {
				UserDao dao = new UserDao();
				dao.deluser(Integer.parseInt(id));
				response.sendRedirect("UserListServlet");
			}catch (Exception e   ) {
				request.setAttribute("errMsg", "削除処理に失敗しました。");
				int dbId = Integer.parseInt(id);
				// DB
				logindao userDao = new logindao();
				User user = userDao.findById(dbId);

				// Servletのデータ受け取り
				request.setAttribute("fromServlet",user);
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/UserDelete.jsp");
				dispatcher.forward(request, response);
				return;
			}
		}else {
			// error
			int dbId = Integer.parseInt(id);
			// DB
			logindao userDao = new logindao();
			User user = userDao.findById(dbId);

			// Servletのデータ受け取り
			request.setAttribute("fromServlet",user);

			// デリートがめんjspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/UserDelete.jsp");
			dispatcher.forward(request, response);
			return;
		}

	}

}

