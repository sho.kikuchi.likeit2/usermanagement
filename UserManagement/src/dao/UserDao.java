package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.User;

/**
 * ユーザテーブル用のDao
 *
 *
 */
public class UserDao {

	/**
	 * ログインIDとパスワードに紐づくユーザ情報を返す
	 * @param loginId
	 * @param password
	 * @return
	 */
	public User findByLoginInfo(String loginid, String password) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginid);
			pStmt.setString(2, password);
			ResultSet rs = pStmt.executeQuery();

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			// 必要なデータのみインスタンスのフィールドに追加
			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("name");
			return new User(loginIdData, nameData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public List<User> findBysearch(String loginid, String name, String birthDateFrom, String birthDateTo) {
		Connection conn = null;
		//User[] userArray = new User[10];
		List<User> userList = new ArrayList<User>();
		try {
			// データベースへ接続
			conn = DBManager.getConnection();
			// SELECT文を準備
			StringBuilder sp = new StringBuilder();
			sp.append("SELECT * FROM user WHERE login_id <> 'admin'");
			if (loginid.length() > 0) {
				//sp.append("login_id = ? ");
				sp.append(" AND login_id = '" + loginid + "'");
			}
			if (name.length() > 0) {
				sp.append(" AND ");
				sp.append("name = '" + name + "'");
				//sp.append("name = " ?");
				//flg2 = true;
			}
			if (birthDateFrom.length() > 0) {
				//if (flg2)
				//sp.append(" AND ");
				//else if (flg1)
				sp.append(" AND ");
				sp.append("birth_date >= '" + birthDateFrom + "'");
				//sp.append("birth_date >= ? '");
				//= true;
			}
			if (birthDateTo.length() > 0) {
				//if (flg3)
				//sp.append(" AND ");
				//else if (flg1 || flg2)
				sp.append(" AND ");
				sp.append("birth_date <= '" + birthDateFrom + "'");
				//sp.append(" ? >= birth_date '");
				//flg4 = true;
			}
			System.out.println(sp.toString());
			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sp.toString());
			//			int count = 0;
			//			if (flg1) {
			//				pStmt.setString(count + 1, loginid);
			//				count = count + 1;
			//			}
			//			if (flg2) {
			//				pStmt.setString(count + 1, name);
			//				count = count + 1;
			//			}
			//			if (flg3) {
			//				pStmt.setString(count + 1, birthDateFrom);
			//				count = count + 1;
			//			}
			//			if (flg4) {
			//				pStmt.setString(count + 1, birthDateTo);
			//				count = count + 1;
			//			}

			ResultSet rs = pStmt.executeQuery();

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name2 = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				User user = new User(id, loginId, name2, birthDate, password, createDate, updateDate);

				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}

		return userList;
	}

	/**
	 * 全てのユーザ情報を取得する
	 * @return
	 */
	public List<User> findAll() {
		Connection conn = null;
		List<User> userList = new ArrayList<User>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			// TODO: 未実装：管理者以外を取得するようSQLを変更する
			String sql = "SELECT * FROM user where login_id <> 'admin'";

			// SELECTを実行し、結果表を取得
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);

				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}

	//delete
	public void deluser(int id) throws Exception {
		Connection con = null;
		PreparedStatement stmt = null;
		try {
			con = DBManager.getConnection();
			String delSQL = "DELETE FROM user WHERE id = ? ";
			stmt = con.prepareStatement(delSQL);
			//stmt.setString(1, id);
			stmt.setInt(1, id);
			int num = stmt.executeUpdate();
			if (num == 0) {
				throw new Exception("削除対象となるIdが不一致で削除失敗");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null) {
					stmt.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	//public User deluser(String id, String password) {
	public User deluser(String loginid, String password) {
		Connection con = null;
		PreparedStatement stmt = null;
		try {
			con = DBManager.getConnection();
			//String delSQL = "DELETE* FROM user WHERE id = ? AND password = ?";
			//DELETE FROM	表名 WHERE	条件
			String delSQL = "DELETE FROM user WHERE login_id = ? AND password = ?";
			stmt = con.prepareStatement(delSQL);
			//stmt.setString(1, id);
			stmt.setString(1, loginid);
			stmt.setString(2, password);
			stmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null) {
					stmt.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	public void UserCreate(String loginid, String name, String birthDate, String password) {
		//public void UserCreate(User insertParams) {

		Connection con = null;
		PreparedStatement stmt = null;

		try {
			con = DBManager.getConnection();
			//String SQL = "DELETE* FROM user WHERE id = ? AND password = ?"
			//DELETE FROM	表名 WHERE	条件
			//String SQL = "INSERT INTO user (id,login_id , birth_date , password , create_date , update_date VALUES (?,?,?,?,now(),now())";
			String SQL = "INSERT INTO user (login_id, name,birth_date ,password,create_date,update_date ) VALUES (?, ?,?, ?,now(),now())";
			stmt = con.prepareStatement(SQL);
			//stmt.setString(1, id);
			stmt.setString(1, loginid);
			stmt.setString(2, name);
			stmt.setString(3, birthDate);
			stmt.setString(4, password);
			//ハッシュを生成したい元の文字列
			//String source = "password";
			//ハッシュ生成前にバイト配列に置き換える際のCharset
			//Charset charset = StandardCharsets.UTF_8;
			//ハッシュアルゴリズム
			//String algorithm = "MD5";
			//ハッシュ生成処理
			//byte[] bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
			//String result = DatatypeConverter.printHexBinary(bytes);
			//標準出力
			//System.out.println(result);
			stmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null) {
					stmt.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	//public void UserUpdate(String id, String loginid, String name, String birthDate, String password) {
	public void UserUpdate(String id, String name, String birthDate, String password) {
		Connection con = null;
		PreparedStatement stmt = null;
		try {
			con = DBManager.getConnection();
			//String SQL = "DELETE* FROM user WHERE id = ? AND password = ?"
			//DELETE FROM	表名 WHERE	条件

			String SQL = "UPDATE user SET birth_date=?,password =?,name=? ,update_date =now() where id = ?";
			stmt = con.prepareStatement(SQL);
			//stmt.setString(1, id);
			//stmt.setString(1, loginid);
			stmt.setString(1, birthDate);
			stmt.setString(2, password);
			stmt.setString(3, name);
			stmt.setString(4, id);
			stmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null) {
					stmt.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public void UserUpPassNull(String id, String name, String birthDate) {
		Connection con = null;
		PreparedStatement stmt = null;
		try {
			con = DBManager.getConnection();
			//String SQL = "DELETE* FROM user WHERE id = ? AND password = ?"
			//DELETE FROM	表名 WHERE	条件

			String SQL = "UPDATE user SET birth_date=?,name=? ,update_date =now() where id = ?";
			stmt = con.prepareStatement(SQL);
			stmt.setString(1, birthDate);
			stmt.setString(2, name);
			stmt.setString(3, id);
			stmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null) {
					stmt.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}
