<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="model.User"%>
<%
	// Servletのデータ受け取り
	request.setCharacterEncoding("UTF8");
    User user = (User) request.getAttribute("fromServlet");
%>

<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>ログイン画面</title>
<!-- BootstrapのCSS読み込み -->
<link href="css/bootstrap.min.css" rel="stylesheet">
<!-- オリジナルCSS読み込み -->
<link href="css/original/common.css" rel="stylesheet">
<!-- Jqeryの読み込み -->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js">
<!-- BootstrapのJS読み込み -->
	<script src="js/bootstrap.min.js">
</script>
<!-- レイアウトカスタマイズ用個別CSS -->

</head>
<body>

	<!-- header -->
	<header>
		<nav class="navbar navbar-inverse">
			<div class="container">
				<div class="navbar-header">
					<a class="navbar-brand" href="UserListServlet">ユーザ管理システム</a>
				</div>
				<ul class="nav navbar-nav navbar-right">
					<li class="navbar-text">${userInfo.name}さん</li>
					<li class="dropdown"><a href="LogoutServlet"
						class="navbar-link logout-link">ログアウト</a></li>
				</ul>
			</div>
		</nav>
	</header>
	<!-- /header -->
	<!-- body -->
	<div class="container">
		<!--<div class="card card-container"></div> -->

		<!-- エラーメッセージのサンプル(エラーがある場合のみ表示) -->
		<c:if test="${errMsg != null}" >
		    <div class="alert alert-danger" role="alert">
			  ${errMsg}
			</div>
		</c:if>

		<form method="post" action="UserUpdateServlet" class="form-horizontal">
			<div class="form-group row">
				<label for="loginId" class="col-sm-2 col-form-label">ログインID</label>
				<div class="col-sm-10">
					<p class="form-control-plaintext"><%= user.getLoginId() %></p>
					<input type="hidden" name="id" value="<%= user.getId() %>">
				</div>
			</div>

			<div class="form-group row">
				<label for="password" class="col-sm-2 col-form-label">パスワード</label>
				<div class="col-sm-10">
					<input type="password" class="form-control" id="password" name="password">
				</div>
			</div>

			<div class="form-group row">
				<label for="passwordConf" class="col-sm-2 col-form-label">パスワード(確認)</label>
				<div class="col-sm-10">
					<input type="password" class="form-control" id="passwordConf" name="passwordConf">
				</div>
			</div>

			<div class="form-group row">
				<label for="userName" class="col-sm-2 col-form-label">ユーザ名</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" id="userName" name="userName" value="<%= user.getName() %>">
				</div>
			</div>

			<div class="form-group row">
				<label for="birthDate" class="col-sm-2 col-form-label">生年月日</label>
				<div class="col-sm-10">
					<input type="date" class="form-control" id="birthDate" name="birthDate" value="<%= user.getBirthDate()  %>">
				</div>
			</div>

			<div class="submit-button-area">
				<button type="submit" value="検索"
					class="btn btn-primary btn-lg btn-block">更新</button>
			</div>
			<div class="submit-button-area">
		<input type="button" class="btn btn-success btn-lg btn-block" onclick="history.back();" value="戻る">
			</div>
		</form>

</body>
</html>
