<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ page import="model.User"%>
<%
	// Servletのデータ受け取り
	request.setCharacterEncoding("UTF8");
	User user = (User) request.getAttribute("fromServlet");
%>

<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>ユーザ詳細画面</title>
<!-- BootstrapのCSS読み込み -->
<link href="css/bootstrap.min.css" rel="stylesheet">
<!-- オリジナルCSS読み込み -->
<link href="css/original/common.css" rel="stylesheet">
<!-- Jqeryの読み込み -->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js">
<!-- BootstrapのJS読み込み -->
	<script src="js/bootstrap.min.js">
</script>
<!-- レイアウトカスタマイズ用個別CSS -->

</head>
<body>

	<!-- header -->
	<header>
		<nav class="navbar navbar-inverse">
			<div class="container">
				<c:if test="${errMsg != null}">
					<div class="alert alert-danger" role="alert">${errMsg}</div>>
			  </c:if>
				>
				<div class="navbar-header"></div>
				<ul class="nav navbar-nav navbar-right">
					<li class="navbar-text">${userInfo.name}さん</li>
					<li class="dropdown"><a href="LogoutServlet"
						class="navbar-link logout-link">ログアウト</a></li>
				</ul>
			</div>
		</nav>
	</header>
	<!-- /header -->

	<!-- body -->

		<div class="container">
			<form method="post" action="#" class="form-horizontal">
				<div class="form-group row">
					<label for="loginId" class="col-sm-2 col-form-label">ログインID</label>
					<div class="col-sm-10">
						<p class="form-control-plaintext">
							<%=user.getLoginId()%></p>
					</div>
				</div>

				<div class="form-group row">
					<label for="userName" class="col-sm-2 col-form-label">ユーザ名</label>
					<div class="col-sm-10">
						<p class="form-control-plaintext"><%=user.getName()%></p>
					</div>
				</div>

				<div class="form-group row">
					<label for="birthDate" class="col-sm-2 col-form-label">生年月日</label>
					<div class="col-sm-10">
						<p class="form-control-plaintext"><%=user.getBirthDate()%></p>
					</div>
				</div>

				<div class="form-group row">
					<label for="createDate" class="col-sm-2 col-form-label">新規登録日時</label>
					<div class="col-sm-10">
						<p class="form-control-plaintext"><%=user.getCreateDate()%></p>
					</div>
				</div>

				<div class="form-group row">
					<label for="updateDate" class="col-sm-2 col-form-label">更新日時</label>
					<div class="col-sm-10">
						<p class="form-control-plaintext"><%=user.getCreateDate()%></p>
					</div>
				</div>

			<div class="submit-button-area">
		<input type="button" class="btn-success  btn-lg btn-block" onclick="history.back();" value="戻る">

				</div>
			</form>


		</div>
</body>
</html>
