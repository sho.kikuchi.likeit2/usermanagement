<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ page import="model.User"%>
<%
	// Servletのデータ受け取り
	request.setCharacterEncoding("UTF8");
    User user = (User) request.getAttribute("fromServlet");
%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>ログイン画面</title>
<!-- BootstrapのCSS読み込み -->
<link href="css/bootstrap.min.css" rel="stylesheet">
<!-- オリジナルCSS読み込み -->
<link href="css/original/common.css" rel="stylesheet">
<!-- Jqeryの読み込み -->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js">
<!-- BootstrapのJS読み込み -->
	<script src="js/bootstrap.min.js">
</script>
<!-- レイアウトカスタマイズ用個別CSS -->

</head>
<body>

	<!-- header -->
	<header>
		<nav class="navbar navbar-inverse">
			<div class="container">
				<div class="navbar-header">
					<a class="navbar-brand" href="UserListServlet">ユーザ管理システム</a>
				</div>
				<ul class="nav navbar-nav navbar-right">
					<li class="navbar-text">${userInfo.name}さん</li>
					<li class="dropdown"><a href="LogoutServlet"
						class="navbar-link logout-link">ログアウト</a>
				</ul>

			</div>
		</nav>
	</header>
	<!-- /header -->

	<!-- body -->
	<div class="container">
		<p><%= user.getLoginId() %> を消去しますか？</p>
		<div class="card card-container">
		<form method="post" action="UserDeleteServlet" class="form-horizontal">
			<input type="button" class="btn btn-danger" onclick="history.back();" value="いいえ">
			<input type="submit" class="btn btn-primary" value="はい">
			<!-- <a href="UserListServlet" class="btn btn-light">いいえ</a><a href="avascript:void(0);" class="btn btn-primary">はい</a> -->
			<input type="hidden" name="id" value="<%= user.getId() %>">
		</form>
		</div>
		<!-- /card-container -->
	</div>
	<!-- /container -->

</body>
</html>